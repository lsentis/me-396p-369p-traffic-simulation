class TrafficSignal:

	def __init__(self, faces, sensitivity):
		self.__faces = faces
		if faces < 0:
			raise Exception('Number of faces is negative')

		for ii in range(0, faces ):
			print 'Creating a new face for the traffic signal'

	def getFaces(self):
		return self.__faces

	def setPeriod(self, seconds):
		self.__period = seconds

	def getPeriod(self):
		return self.__period

	# nested manager class
	class Manager:
		def __init__(self):
			self.__listTrafficSignals = []

		def newTrafficSignal(self,faces, sensitivity):
			# handle general exceptions
			try:
				ts = TrafficSignal(faces, sensitivity)
				self.__listTrafficSignals.append( ts )

			except Exception, e:
				print 'Exception on TrafficSignal.Manager:', e

		def getTrafficSignals(self):
			return self.__listTrafficSignals