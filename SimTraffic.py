from TrafficSignal import *

class SimTraffic:

	def __init__(self):
		pass

	def newSimulation(self):
		# create traffic signal with multiple faces
		man = TrafficSignal.Manager()
		man.newTrafficSignal('a',40)
		man.newTrafficSignal(4,40)

		print 'Num of traffic signals created is:', len( man.getTrafficSignals() )
		# registers signals for state changes

		# handle signal and timer notifications

# main
SimTraffic().newSimulation()	